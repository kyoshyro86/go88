/**
 * Created by Nofear on 6/8/2017.
 */
(function () {
    cc.SubdomainName = cc.Enum({
        PORTAL: "portal.", //root
        EVENT: "event.",
        VQMM: "vqmm.",
        CHAT: "chat.",
        TREASURE: "treasure.",

        SAFE_OTP: "safeotp.",

        EGYPT: "egypt.",
        THREE_KINGDOM: "tamquoc.",
        AQUARIUM: 'thuycung.',
        DRAGON_BALL: 'songoku.',
        BUM_BUM: 'bumbum.',
        COWBOY: 'cowboys.',
        THUONGHAI: 'demthuonghai.',

        MONKEY: 'monkey.',
        XOC_XOC: 'xocdia.',
        DRAGON_TIGER: "dragon.",
        BACCARAT: 'baccarat.',
        BAUCUA: 'baucua.',

        THREE_CARDS: 'bacay.',
        TEXAS_POKER: 'poker.',
        TLMN: "tlmn.",
        TLMN_SOLO: 'tlmnsolo.',
        MAU_BINH: 'maubinh.',

        LODE: 'xoso.',
        VIETLOT: 'vietlott.',

        SEVEN77: "minibar.",
        MINI_POKER: "minipoker.",
        BLOCK_BUSTER: "blockbuster.",
        TAI_XIU: "taixiu.",
		TAI_XIU_MD5: "taixiumd5.",
		SICBO: "sicbo.",
        LUCKY_WILD: "luckywild.",

        SHOOT_FISH: 'shootfish.',
        GAINHAY: 'gainhay.',



        // PORTAL: "portal2.",
        // EVENT: "event2.",
        // VQMM: "vqmm2.",
        // CHAT: "chat.",
        // TREASURE: "treasure.",

        // SAFE_OTP: "safeotp2.",

        // EGYPT: "egypt2.",
        // THREE_KINGDOM: "tamquoc2.",
        // AQUARIUM: 'thuycung2.',
        // DRAGON_BALL: 'songoku2.',
        // BUM_BUM: 'bumbum2.',
        // COWBOY: 'cowboy2.',

        // MONKEY: 'monkey2.',
        // XOC_XOC: 'xocxoc.',
        // DRAGON_TIGER: "dragon.",

        // SEVEN77: "minibar2.",
        // MINI_POKER: "minipoker2.",
        // BLOCK_BUSTER: "blockbuster2.",
        // TAI_XIU: "taixiu2.",
        // LUCKY_WILD: "luckywild2.",

        // THREE_CARDS: 'bacay.',
        // TEXAS_POKER: 'poker.',
        // TLMN: "tlmn.",
        // TLMN_SOLO: 'tlmnsolo.',
        // MAU_BINH: 'maubinh.',
        // BACCARAT: 'baccarat.',
        // BAUCUA: 'baucua.',

        // SHOOT_FISH: 'shootfish.'




        // PORTAL: "portal3.",
        // EVENT: "event3.",
        // VQMM: "vqmm3.",
        // CHAT: "chat.",
        // TREASURE: "treasure.",

        // SAFE_OTP: "safeotp3.",

        // EGYPT: "egypt3.",
        // THREE_KINGDOM: "tamquoc3.",
        // AQUARIUM: 'thuycung3.',
        // DRAGON_BALL: 'songoku3.',
        // BUM_BUM: 'bumbum3.',
        // COWBOY: 'cowboy3.',

        // MONKEY: 'monkey3.',
        // XOC_XOC: 'xocxoc.',
        // DRAGON_TIGER: "dragon.",

        // SEVEN77: "minibar3.",
        // MINI_POKER: "minipoker3.",
        // BLOCK_BUSTER: "blockbuster3.",
        // TAI_XIU: "taixiu3.",
        // LUCKY_WILD: "luckywild3.",

        // THREE_CARDS: 'bacay.',
        // TEXAS_POKER: 'poker.',
        // TLMN: "tlmn.",
        // TLMN_SOLO: 'tlmnsolo.',
        // MAU_BINH: 'maubinh.',
        // BACCARAT: 'baccarat.',
        // BAUCUA: 'baucua.',

        // SHOOT_FISH: 'shootfish.'
    });

}).call(this);
