/**
 * Created by Nofear on 3/20/2019.
 */

(function () {
    cc.DDNAUILocation = cc.Enum({
        PORTAL: 'PORTAL',
        HISTORY: 'HISTORY',
        SHOP: 'SHOP',
        ACCOUNT_INFO: 'ACCOUNT_INFO',

        THREE_KINGDOM: 'THREE_KINGDOM',
        BLOCK_BUSTER: 'BLOCK_BUSTER',
        AQUARIUM: 'AQUARIUM',
        DRAGON_BALL: 'DRAGON_BALL',
        BUM_BUM: 'BUM_BUM',

        MONKEY: 'MONKEY',
        DRAGON_TIGER: 'DRAGON_TIGER',

        TAI_XIU: 'TAI_XIU',
		TAI_XIU_MD5: 'TAI_XIU_MD5',
        EGYPT: 'EGYPT',
        SEVEN77: 'SEVEN77',
        MINI_POKER: 'MINI_POKER',
        VQMM: 'VQMM',
        THUONGHAI: 'THUONGHAI',
        GAINHAY: 'GAINHAY'
    });

}).call(this);
